<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoSubmissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('youtube_url');
            $table->string('video_title');
            $table->text('video_description')->nullable();
            $table->string('video_created_by');
            $table->dateTime('video_submitted');
            $table->integer('likes')->default(0);
            $table->integer('views')->default(0);
            $table->integer('posted_by')->unsigned();
            $table->timestamps();

            $table->foreign('posted_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_submissions');
    }
}
