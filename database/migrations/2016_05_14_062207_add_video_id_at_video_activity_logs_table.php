<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideoIdAtVideoActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_activity_logs', function (Blueprint $table) {
            $table->integer('video_id')->unsigned()->after('device');

            $table->foreign('video_id')->references('id')->on('video_submissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_activity_logs', function (Blueprint $table) {
            $table->dropForeign('video_activity_logs_video_id_foreign');
            $table->dropColumn('video_id');
        });
    }
}
