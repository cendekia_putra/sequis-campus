<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYoutubeIdAtVideoSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_submissions', function (Blueprint $table) {
            $table->string('youtube_id')->nullable()->after('youtube_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_submissions', function (Blueprint $table) {
            $table->dropColumn('youtube_id');
        });
    }
}
