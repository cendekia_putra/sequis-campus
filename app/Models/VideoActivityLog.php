<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoActivityLog extends Model
{
    public static function addLog($type, $userId = null, $videoId)
    {
    	$log = new self;
    	$log->type = $type;
    	$log->ip_address = \Request::ip();
    	$log->user_id = $userId;
    	$log->video_id = $videoId;

    	return $log->save();
    }
}
