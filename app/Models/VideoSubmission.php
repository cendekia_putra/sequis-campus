<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoSubmission extends Model
{
    public function logs()
    {
    	return $this->hasMany(VideoAvtivityLog::class);
    }
}
