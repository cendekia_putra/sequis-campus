<?php

namespace App\Http\Middleware;

use Closure;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            if (\Auth::user()->id != 1) {
                return redirect(env('APP_URL'));
            }
        } elseif (!\Auth::check()) {
            return redirect(env('APP_URL'));
        }

        return $next($request);
    }
}
