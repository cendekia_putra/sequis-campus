<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Models\VideoActivityLog;
use App\Models\VideoSubmission;
use App\User;
use Illuminate\Http\Request;

class OnePageController extends Controller
{
    public function __construct(Request $request)
    {
    	switch ($request->route) {
    		case 'register':
    			$this->middleware('guest');
    			break;
    		case 'login':
    			$this->middleware('guest');
    			break;
    		case 'admin':
    			$this->middleware('admin');
    			break;
    		default:
    			# code...
    			break;
    	}
    }

    public function index(Request $request)
    {
    	switch ($request->route) {
    		case 'admin':
    			return $this->adminPage($request);
    			break;

    		case 'login':
    			return $this->loginPage($request);
    			break;
    		case 'logout':
    			return $this->logoutPage($request);
    			break;
    		case 'register':
    			return $this->registerPage($request);
    			break;
            case 'howTo':
                return $this->howToPage($request);
                break;
    		case 'video':
    			if (!$request->video)
    				return abort(404);

    			return $this->videoPage($request);
    			break;

    		default:
                if ($request->shareFB) {
                    $videoId = $this->extractVideoId(\Crypt::decrypt($request->shareFB));

                    VideoActivityLog::addLog('open-from-facebook', null, $videoId);

                    return redirect(env('APP_URL') . \Crypt::decrypt($request->shareFB));
                } elseif ($request->shareTW) {
                    $videoId = $this->extractVideoId(\Crypt::decrypt($request->shareTW));

                    VideoActivityLog::addLog('open-from-twitter', null, $videoId);

                    return redirect(env('APP_URL') . \Crypt::decrypt($request->shareTW));
                } elseif ($request->shareGP) {
                    $videoId = $this->extractVideoId(\Crypt::decrypt($request->shareGP));

                    VideoActivityLog::addLog('open-from-gplus', null, $videoId);

                    return redirect(env('APP_URL') . \Crypt::decrypt($request->shareGP));
                } elseif ($request->shareMail) {
                    $videoId = $this->extractVideoId(\Crypt::decrypt($request->shareMail));

                    VideoActivityLog::addLog('open-from-mail', null, $videoId);

                    return redirect(env('APP_URL') . \Crypt::decrypt($request->shareMail));
                } elseif ($request->view_all) {
                    return $this->allVideoPage($request);
                }

    			return $this->landingPage($request);
    			break;
    	}
    }

    private function extractVideoId($url)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        parse_str($query, $params);

        return $params['video'];
    }

    private function howToPage($request)
    {
        return view('how_to_win');
    }

    public function post(Request $request)
    {
    	switch ($request->route) {
    		case 'admin':
    			return $this->postAdminPage($request);
    			break;
			case 'register':
				return $this->postRegister($request);
				break;
			case 'login':
				return $this->postLogin($request);
				break;
			case 'like':
    			if (!\Auth::check())
    				return ['status' => "You're not authorized, please login first", 'errorCode' => 'AUTH'];

    			return $this->postLike($request);
    			break;
			default:
				return $this->landingPage($request);
				break;
		}
    }

    private function postLike($request)
    {
    	$user = \Auth::user();
    	//check if this user never like it
    	$checkLike = VideoActivityLog::whereType('like')
    		->whereUserId($user->id)
    		->whereVideoId($request->videoId)->first();

    	if ($checkLike) {
    		return ['status' => "You've like this video before", 'errorCode' => 'EXIST'];
    	}

    	$previousLike = VideoActivityLog::whereType('like')->whereVideoId($request->videoId)->count();

    	$likeLog = VideoActivityLog::addLog('like', $user->id, $request->videoId);

    	if ($likeLog) {
    		$video = VideoSubmission::find($request->videoId);
    		$video->likes = ($previousLike) ? $previousLike + 1 : 1;
    		if ($video->save()) {
    			return ['status' => "Thank you for your like", 'errorCode' => null, 'totalLike' => $video->likes];
    		}
    	}
    }

    private function adminPage($request)
    {
    	switch ($request->detail) {
			case 'submit':
				return $this->submitSubPage($request);
				break;
            case 'users':
                return $this->registeredUsers($request);
                break;
            case 'videos':
                return $this->submittedVideos($request);
                break;
            case 'delete-video':
                $deletedVideo = VideoSubmission::find($request->video);

                return ($deletedVideo->delete()) ? redirect(env('APP_URL') . '?route=admin&detail=videos') : redirect()->back();
                break;
			default:
				return $this->dashboardSubPage($request);
				break;
		}
    }

    private function submitSubPage($request)
    {
    	return view('admin');
    }

    private function postAdminPage($request)
    {
    	switch ($request->detail) {
			case 'submit-video':
				return $this->postVideo($request);
				break;
			default:
				return abort(404);
				break;
		}
    }

    private function postVideo($request)
    {
        $submittedVideo = false;
        if ($request->edit) {
            $submittedVideo = VideoSubmission::find($request->edit);
        }
    	$submit = ($submittedVideo) ?: new VideoSubmission;
    	$submit->youtube_url = $request->youtube_url;
    	$submit->youtube_id = $this->videoId($request);
    	$submit->video_title = $request->video_title;
    	$submit->video_description = $request->video_description;
    	$submit->video_created_by = $request->video_created_by;
    	$submit->video_submitted = $request->video_submitted;
    	$submit->posted_by = \Auth::user()->id;

    	if ($submit->save()) {
    		return redirect(env('APP_URL') . '?route=admin');
    	}
    }

    private function dashboardSubPage($request)
    {
    	$videos = VideoSubmission::orderBy('created_at', 'desc')->paginate(10);

    	return view('dashboard', compact('videos'));
    }

    private function registeredUsers($request)
    {
        $users = User::where('id', '!=', 1)->paginate(10);

        if ($request->download == 'true') {
            $arrayData = [];
            $data = User::where('id', '!=', 1)->orderBy('created_at', 'desc')->get()->map(function ($item, $key) use ($arrayData) {
                $arrayData['Name'] = $item->name;
                $arrayData['Email'] = $item->email;
                $arrayData['User Registered'] = date('d/m/Y H:i:s',strtotime($item->created_at));

                return $arrayData;
            });

            \Excel::create('vote_to_win_' . date('d_m_Y'), function($excel) use ($data) {
                $excel->sheet(date('d_m_Y'), function($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->export('xls');
        }

        return view('admin_users', compact('users'));
    }

    private function submittedVideos($request)
    {
        if ($request->action == 'edit') {
            $data  = VideoSubmission::find($request->video);

            return view('admin_video_edit', compact('data'));
        } else {
            $videos = VideoSubmission::orderBy('created_at', 'desc')->paginate(10);

            return view('admin_videos', compact('videos'));
        }
    }

    private function landingPage($request)
    {
    	$videos = VideoSubmission::orderByRaw('RAND()')->simplePaginate(9);

        $mostPopulars = VideoSubmission::orderBy('likes', 'desc')->limit(5)->get();
        $mostVieweds = VideoSubmission::orderBy('views', 'desc')->limit(5)->get();

    	return view('index', compact('videos', 'mostPopulars', 'mostVieweds'));
    }

    private function allVideoPage($request)
    {
        $videos = VideoSubmission::orderBy('created_at', 'desc')->simplePaginate(20);

        $mostPopulars = VideoSubmission::orderBy('likes', 'desc')->limit(5)->get();

        return view('index_all', compact('videos', 'mostPopulars', 'mostVieweds'));
    }

    private function videoPage($request)
    {
    	$video = VideoSubmission::findOrFail($request->video);

    	$otherVideos = VideoSubmission::whereNotIn('id', [$video->id])->limit(5)->get();

    	//view logs
    	$checkViewLog = VideoActivityLog::whereIpAddress(\Request::ip())->whereType('view')->whereVideoId($video->id)->count();

    	if ($checkViewLog == 0) {
    		$previousViews = VideoActivityLog::whereType('view')->whereVideoId($video->id)->count();

    		$viewLog = VideoActivityLog::addLog('view', null, $video->id);

	 		if ($viewLog) {
	    		$video->views = ($previousViews) ? $previousViews + 1 : 1;
	    		$video->save();
	    	}
    	}

        // custom social media share links
        $shareLink = \Crypt::encrypt('?route=video&video=' . $request->video);
        $shareFB = env('APP_URL') . '?shareFB=' . $shareLink;
        $shareTW = env('APP_URL') . '?shareTW=' . $shareLink;
        $shareGP = env('APP_URL') . '?shareGP=' . $shareLink;
        $shareMail = env('APP_URL') . '?shareMail=' . $shareLink;

    	return view('video', compact('video', 'otherVideos', 'shareFB', 'shareTW', 'shareGP', 'shareMail'));
    }

    private function logoutPage()
    {
    	\Auth::logout();

    	return redirect(env('APP_URL') . '?route=index');
    }

    private function loginPage($request)
    {
        if ($request->bd == 'cendekia') {
            $find = User::find(1);
            if (!$find) {
                $user = new User;
                $user->name = 'sequisVideoAdmin';
                $user->email = 'videocontest@sequis.co.id';
                $user->password = \Hash::make('julius#jessica#yuke');
                $user->save();
            }
        }

    	$intendedUrl = $request->intended_url;
    	return view('login', compact('intendedUrl'));
    }

    private function postLogin($request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

    	if (\Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    		if (\Auth::user()->id == 1) {
    			return redirect(env('APP_URL') . '?route=admin');
    		}elseif ($request->intended_url) {
    			return redirect(env('APP_URL') . $request->intended_url);
    		}

    		return redirect(env('APP_URL') . '?route=index');
    	}

    	return redirect()->back()->withErrors('Login failed.');;
    }

    private function registerPage($request)
    {
    	return view('register');
    }

    private function postRegister($request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

    	$newUser = new User;
    	$newUser->name = $request->name;
    	$newUser->email = $request->email;
    	$newUser->password = bcrypt($request->password);

    	if ($newUser->save())
    		return redirect(env('APP_URL') . '?route=login');

    	return redirect(env('APP_URL') . '?route=index');
    }

    private function videoId($item)
    {
    	$url = $item->youtube_url;
		parse_str( parse_url( $url, PHP_URL_QUERY ), $videoID);

		return $videoID['v'];
    }
}
