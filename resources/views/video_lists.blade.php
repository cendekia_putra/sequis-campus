@extends('layout')

@section('content')
	<!--Banner Start-->
			<div class="cp_inner-banner">
				<div class="container">
					<div class="cp-inner-banner-holder">
						<h2>Admin Page</h2>
						<ul class="breadcrumb">
							<li><a href="{{ env('APP_URL') }}?route=admin">Dashboard</a></li>
							<li><a href="{{ env('APP_URL') }}?route=admin&detail=video">Videos</a></li>
							<li><a href="{{ env('APP_URL') }}?route=admin&detail=submit">Submit Video</a></li>
						</ul>
					</div>
				</div>
			</div><!--Banner End-->

			<!--Main Content Start-->
			<div id="cp-main-content">
				<!--Login Section Start-->
				<section class="cp-login-section pd-tb60">
					<div class="container">
						<!--Form Box Start-->
       					<div class="cp-form-box cp-form-box2">
       						<h3>Submit Video</h3>
       						<form action="{{ env('APP_URL') }}?route=admin" method="post">
       							{{ csrf_field() }}
       							<div class="row">
		       						<div class="col-md-12 col-sm-12">
		       							<div class="inner-holder">
		       								<h3>Youtube URL*</h3>
		       								<input type="text" placeholder="Youtube URL" name="youtube_url" required>
		       							</div>
		       						</div>
		       						<div class="col-md-12 col-sm-12">
		       							<h3>Title*</h3>
       									<div class="inner-holder">
		       								<input type="text" placeholder="Video Title" name="video_title" required>
		       							</div>
		       						</div>
		       						<div class="col-md-12 col-sm-12">
		       							<h3>Description*</h3>
       									<div class="inner-holder">
       										<textarea name="video_description"></textarea>
		       							</div>
		       						</div>
		       						<div class="col-md-6 col-sm-6">
		       							<h3>Created By*</h3>
       									<div class="inner-holder">
		       								<input type="text" placeholder="Created By" name="video_created_by" required>
		       							</div>
		       						</div>
		       						<div class="col-md-6 col-sm-6">
		       							<h3>Date Submitted*</h3>
       									<div class="inner-holder">
		       								<input type="text" placeholder="Date Submitted" name="video_submitted" required>
		       							</div>
		       						</div>

       								<div class="col-md-6 col-sm-6 col-xs-4">
       									<div class="inner-holder">
       										<button type="submit" class="btn-submit" value="Submit">Submit</button>
       									</div>
       								</div>
       							</div>
       						</form>
       					</div><!--Form Box End-->
					</div>

				</section><!--Login Section End-->
			</div><!--Main Content End-->
@endsection