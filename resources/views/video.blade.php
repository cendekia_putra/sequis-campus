@extends('layout')

@section('content')
	<!--Banner Start-->
    <div class="cp_inner-banner">
        <div class="container">
            <div class="cp-inner-banner-holder">
                <h2>{{ $video->video_title }}</h2>
                <!--Breadcrumb Start-->
                <ul class="breadcrumb">
                    <li><span>Created by : </span>{{ $video->video_created_by }}</li>
                </ul><!--Breadcrumb End-->
            </div>
        </div>
    </div><!--Banner End-->

    <!--Main Content Start-->
    <div id="cp-main-content">
        <section class="cp-blog-section pd-tb60">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <!--Video Detail Outer Start-->
                        <div class="cp-video-detail-outer" style="margin-bottom: 20px;">
                            <div class="cp-video-outer2">
                                <iframe src="https://www.youtube.com/embed/{{ $video->youtube_id }}?rel=0"></iframe>
                            </div>
                            <div class="cp-text-holder" style="min-height: 280px;">
                                <div class="cp-top" style="padding-bottom: 20px;">
                                    <h4>
                                        {{ $video->video_title }}
                                        <ul class="cp-meta-list" style="margin-top: 10px;font-weight: normal;">
                                            <li>{{ date('l, F d, Y', strtotime($video->video_submitted)) }}</li>
                                            <li>by {{ $video->video_created_by }}</li>
                                        </ul>
                                    </h4>
                                    <span class="viewer">{{ $video->views }} Views</span>
                                </div>
                                <div class="cp-watch-holer mb-0">
                                    <ul class="cp-watch-listed">
                                        <li><a href="javascript:;" onclick="openShareBtn()"><i class="fa fa-share"></i> Share</a>
                                            <ul class="cp-social-links shareBtn">
                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ $shareFB }}" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                                <li><a href="https://twitter.com/share?url={{ $shareTW }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="https://plus.google.com/share?url={{ $shareGP }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#" onclick="javascript:window.location='mailto:?subject=Sequis Online Vote [{{ $video->video_title }}]&body=Please vote the video in this link : {{ $shareMail }}';"><i class="fa fa-envelope"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <div class="cp-viewer-outer">
                                        <a href="javascript:;" onclick="like({{ $video->id }})" style="margin-right: 0px;"><i class="fa fa-thumbs-o-up"></i><span class="totalLike">{{ $video->likes }}</span></a>
                                        <span class="likeLoading" style="display: none;margin-left: 10px;font-size: 12px;color: #99a3b1;">Loading...</span>
                                        <span class="errorLogin" style="display: none;margin-left: 10px;">
                                            <a href="{{ env('APP_URL') }}?route=login&intended_url={{ urlencode('?route=video&video='.$video->id) }}">Please login first.</a>
                                        </span>
                                        <span class="errorLike" style="display: none;margin-left: 10px;font-size: 12px;color: #99a3b1;"></span>
                                    </div>
                                </div>
                                <div class="cp-watch-holer">
                                    <p class="cp-meta-list">
                                        {!! $video->video_description !!}
                                    </p>
                                </div>
                            </div>
                        </div><!--Video Detail Outer End-->

                    </div>
                    <div class="col-md-3">
                        <aside class="cp_sidebar-outer">
                            <!--Widget Item Start-->
                            <div class="widget widget-recent-post">
                                <div class="cp-heading-outer">
                                    <h2>Other Videos</h2>
                                </div>
                                <ul>
                                    @foreach ($otherVideos as $other)
                                    <li>
                                        <div class="cp-holder">
                                            <div class="cp-thumb2">
                                                <a href="{{ env('APP_URL') }}?route=video&video={{ $other->id }}">
                                                <img style="width: 75px" src="http://img.youtube.com/vi/{{ $other->youtube_id }}/default.jpg" alt=""></a>
                                            </div>
                                            <div class="cp-text">
                                                <h5><a href="{{ env('APP_URL') }}?route=video&video={{ $other->id }}">{{ $other->video_title }}</a></h5>
                                                <ul class="cp-meta-list">
                                                    <li>{{ date('l, F d, Y', strtotime($other->video_submitted)) }}</li>
                                                    <li>by {{ $other->video_created_by }}, <span>{{ $other->views }} Views</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="cp-show-more-outer">
                                    <a href="{{ env('APP_URL') . '?route=index' }}">Show More</a>
                                </div>
                            </div><!--Widget Item End-->

                        </aside>
                    </div>
                </div>
            </div>
        </section>
    </div><!--Main Content End-->

    <script>
        var appUrl = "<?php echo env('APP_URL'); ?>";
        var token = "{!! csrf_token() !!}";
        function like(id) {
            openShareBtn(1);
            $('.errorLogin, .errorLike').hide();
            $('.likeLoading').fadeIn();

            $.ajax({
                method: "POST",
                url: appUrl + "?route=like",
                data: { videoId: id, '_token': token}
            })
            .done(function( response ) {
                $('.likeLoading').hide();

                switch (response.errorCode) {
                    case 'AUTH':
                        $('.errorLogin').fadeIn();
                        break;
                    case 'EXIST':
                        $('.errorLike').fadeIn().text(response.status);
                        break;
                    default:
                        $('.totalLike').text(response.totalLike);
                        break;
                }
            });
        }

        function openShareBtn(close){
            if (close) {
                $visibility = 'visibility:hidden';
                $animateAttrs = {
                    'left': '0px',
                    'top': '3px',
                    'min-width': '200px',
                    'z-index': '99',
                    'opacity': '0'
                };
            } else {
                $('.errorLogin, .errorLike, .likeLoading').hide();

                $visibility = 'visibility:visible';
                $animateAttrs = {
                    'left': '70px',
                    'top': '3px',
                    'min-width': '200px',
                    'z-index': '99',
                    'opacity': '1'
                };
            }

            $('.shareBtn').attr('style', $visibility).animate($animateAttrs, 100);
        }
    </script>
@endsection