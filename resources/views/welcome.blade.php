
<!DOCTYPE html>
<html>
<!--  -->
<head>
    <title>Sequis Online</title>
    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Setiap hal besar berawal dari hal kecil. Yuk, bikin video hal-hal kecil yang berarti bagi hidupmu dan bagikan ke teman-temanmu!">
    <meta name="author" content="Sequis Online">
    <meta name="keywords" content="">

    <!-- meta facebook -->
    <meta property="og:url"           content="https://online.sequis.co.id/little-things-matter" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Sequis Online" />
    <meta property="og:description"   content="Setiap hal besar berawal dari hal kecil. Yuk, bikin video hal-hal kecil yang berarti bagi hidupmu dan bagikan ke teman-temanmu!" />
    <meta property="fb:app_id"        content="1744789499085787" />
    <meta property="og:image"         content="https://online.sequis.co.id/little-things-matter/img/fb-share.png" />

    <!-- meta twitter -->
    <meta property="twitter:card"           content="summary_large_image" />
    <meta property="twitter:site"  content="@SequisOFFICIAL"/>
    <meta property="twitter:creator" content="@SequisOFFICIAL"/>
    <meta property="twitter:url"            content="https://online.sequis.co.id/little-things-matter" />
    <meta property="twitter:description"    content="Yuk bikin video hal-hal kecil yang berarti bagi hidupmu dan bagikan ke teman-temanmu! #LittleThingsMatter" />
    <meta property="twitter:title"          content="Sequis Online" />
    <meta property="twitter:image"          content="https://online.sequis.co.id/little-things-matter/img/twitter-card.png" />

    <!-- css -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- jquery -->
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <link rel="shortcut icon" href="favicon.ico">
</head>

<body id="question">
    <header>
        <div class="container">
            <div class="logo pull-left">
                <a href="https://online.sequis.co.id/id"><img src="https://online.sequis.co.id/little-things-matter/img/sequis_logo_b.png"/></a>
            </div>
            <div class="share pull-right">

            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="tonton">
                    <h3>Lorem ipsum dolorem</h3>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="copyright pull-left">
                <small>&copy; 2015. PT. AJ Sequislife terdaftar dan diawasi oleh Otoritas Jasa Keuangan.</small>
            </div>
            <div class="terms pull-right">
                <small>
                    <a href="#" class="show1">Terms & Conditions</a>
                    <a href="http://online.sequis.co.id/id/privasi">Privacy</a>
                    <a href="https://www.facebook.com/sequislife/"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/SequisOFFICIAL"><i class="fa fa-twitter"></i></a>
                </small>
            </div>
        </div>
    </footer>
</body>
</html>


