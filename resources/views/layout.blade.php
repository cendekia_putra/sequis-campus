<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <title>Sequis Video Competition</title>
        <!---Custom CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/custom.css" type="text/css">
        <!---BootStrap CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/bootstrap.css" type="text/css">
        <!---Menu CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/mega-menu.css" type="text/css">
        <!---Theme Color CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/theme-color.css" type="text/css">
        <!---Responsive CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/responsive.css" type="text/css">
        <!---Owl Slider CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/owl.carousel.css" type="text/css">
        <!---BxSlider CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/jquery.bxslider.css" type="text/css">
        <!---Font Awesome CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/font-awesome.min.css" type="text/css">
        <!---PrettyPhoto CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/prettyPhoto.css" type="text/css">
        <!---Audio Player CSS-->
        <link rel="stylesheet" href="{{ env('ASSET_URL') }}css/audioplayer.css" type="text/css">

        <!---Font Family Roboto CSS-->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>

        <!---JQuery-1.11.3.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/jquery-1.11.3.min.js"></script>
    </head>
    <body>

        <!--Wrapper Content Start-->
        <div id="wrapper">
        <!--Header Start-->
            <header class="cp_header">
                <!--Navigation Start-->
                <div class="cp-navigation-row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <!--Logo Start-->
                                <strong class="cp-logo" style="margin: 10px 40px;">
                                    <a href="https://online.sequis.co.id"><img src="{{ env('ASSET_URL') }}img/logo-white.png" alt=""></a>
                                </strong>
                                <!--Logo End-->
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <div class="cp-right-holder">
                                    <ul class="cp-social-links">
                                        <li><i class="fa fa-home"></i> <a href="{{ env('APP_URL') }}">Home</a></li>
                                        @if (\Auth::check())
                                            <?php
                                                $user = \Auth::user();
                                            ?>
                                            <li><i class="fa fa-user"></i> {{ ucwords($user->name) }}</li>
                                            <li><i class="fa fa-lock"></i> <a href="{{ env('APP_URL') }}?route=logout">Logout</a></li>
                                        @else
                                            <li><i class="fa fa-user"></i> <a href="{{ env('APP_URL') }}?route=login">Login</a></li>
                                            <li><i class="fa fa-user"></i> <a href="{{ env('APP_URL') }}?route=register">Register</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--Navigation End-->
            </header><!--Header End-->

            @yield('content')

            <!--Footer Start-->
            <footer class="cp_footer">
                <!--Copyright Section Start-->
                <section class="cp-copyright-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12" style="margin:10px 0;">
                                © 2016. PT. AJ Sequislife terdaftar dan diawasi oleh Otoritas Jasa Keuangan
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" style="margin:10px 0;">
                                <nav class="cp-ft-nav cp-ft-nav2" style="float:right;text-align: right;">
                                    <ul>
                                        <li><a href="#" style="color:#555555;">Term & Condition</a></li>
                                        <li><a href="#" style="color:#555555;">Privacy</a></li>
                                        <li><a href="{{ env('APP_URL') }}?route=howTo" style="color:#555555;">How to Vote</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                </section><!--Copyright Section End-->

            </footer><!--Footer End-->

        </div><!--Wrapper Content End-->

        <!---BootStrap.min.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/bootstrap.min.js"></script>
        <!--HTML5 Js-->
        <script src="{{ env('ASSET_URL') }}js/html5shiv.js" type="text/javascript"></script>
        <!---Migrate.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/migrate.js"></script>
        <!---Owl Carousel Slider.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/owl.carousel.min.js"></script>
        <!---Bx Slider.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/jquery.bxslider.min.js"></script>
        <!---Countdown.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/jquery.countdown.js"></script>
        <!---PrettyPhoto.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/jquery.prettyPhoto.js"></script>
        <!---Custom Script.js-->
        <script type="text/javascript" src="{{ env('ASSET_URL') }}js/custom-script.js"></script>
    </body>
</html>
