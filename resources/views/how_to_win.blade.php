@extends('layout')

@section('content')
	<!--Banner Start-->
	<div class="cp_inner-banner">
		<div class="container">
			<div class="cp-inner-banner-holder">
				<h2>Sequis Online - Vote to Win</h2>
				<ul class="breadcrumb">
                    <li style="color: #00b4c5;font-size: 15px;line-height: 1.5;">Dukung tim favoritmu untuk mendapatkan <strong>golden ticket</strong> menuju babak <strong>Semi Final Marketition 2016!</strong></li>
                </ul><!--Breadcrumb End-->
			</div>
		</div>
	</div><!--Banner End-->

	<!--Main Content Start-->
	<div id="cp-main-content">
		<!--Login Section Start-->
		<section class="cp-register-section pd-tb60">
			<div class="container">
				<!--Form Box Start-->
					<div class="cp-form-box cp-form-box2">
						<h3>How to Vote</h3>
						<ol>
							<li style="list-style: inherit;line-height: 1.8;"><strong>Sign in</strong> dengan meng-klik tombol <strong>Login</strong> di kanan atas halaman ini</li>
							<li style="list-style: inherit;line-height: 1.8;">Pilih video favoritmu, klik pada bagian gambar atau judul video, lalu klik icon <strong>Like</strong> di sisi kanan bawah video</li>
							<li style="list-style: inherit;line-height: 1.8;"><strong>Setiap orang</strong> yang terdaftar dapat meng-klik like <strong>satu kali di masing-masing video</strong></li>
							<li style="list-style: inherit;line-height: 1.8;"><strong>Ajak teman-temanmu</strong> untuk ikut bergabung! Share link video favoritmu ke social media dengan meng-klik tombol <strong>share</strong> di bawah judul video.</li>
							<li style="list-style: inherit;line-height: 1.8;"><strong>3 (tiga)</strong> kelompok pemilik <strong>video dengan likes terbanyak</strong> akan secara <strong>otomatis maju ke babak Semi Final Marketition 2016</strong>, tanpa memperhitungkan nilai paper yang telah dibuat.</li>
						</ol>
						<p><strong>Disclaimer</strong> : Jumlah likes yang tercatat di halaman YouTube <strong>tidak akan diperhitungkan</strong> dalam penilaian dewan juri. <strong>Pastikan kamu sudah login</strong> dan terdaftar di situs ini sebelum melakukan vote.</p>
						<p>Klik dan cari tahu lebih lanjut tentang <a href="https://online.sequis.co.id" target="_blank">Sequis Online</a> dan <a href="http://marketition.id" target="_blank">Marketition 2016!</a></p>
					</div><!--Form Box End-->
			</div>

		</section><!--Login Section End-->
	</div><!--Main Content End-->
@endsection