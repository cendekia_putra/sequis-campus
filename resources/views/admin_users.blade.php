@extends('layout')

@section('content')
	<!--Banner Start-->
			@include('admin_menu')

			<!--Main Content Start-->
			<div id="cp-main-content">
				<!--Login Section Start-->
				<section class="cp-login-section pd-tb60">
					<div class="container">
						<!--Form Box Start-->
       					<div class="cp-form-box cp-form-box2">
       						<h3>Registered Users</h3>
       						<a href="{{ env('APP_URL') }}?route=admin&detail=users&download=true" class="btn btn-success">Download Excel</a>
   							<div class="row">
	       						<div class="col-md-12 col-sm-12">
	       							<div class="inner-holder">
	       								<table class="table table-hover">
	       									<tr>
	       										<th>Name</th>
	       										<th>Email</th>
	       										<th>Register date</th>
	       									</tr>
	       									@foreach ($users as $item)
	       									<tr>
	       										<td>{{ $item->name }}</td>
	       										<td>{{ $item->email }}</td>
	       										<td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
	       									</tr>
	       									@endforeach
	       								</table>
	       								{!! $users->appends(['route' => 'admin', 'detail' => 'users'])->render() !!}
	       							</div>
	       						</div>
   							</div>
       					</div><!--Form Box End-->
					</div>

				</section><!--Login Section End-->
			</div><!--Main Content End-->
@endsection