@extends('layout')

@section('content')
	<!--Banner Start-->
			@include('admin_menu')

			<!--Main Content Start-->
			<div id="cp-main-content">
				<!--Login Section Start-->
				<section class="cp-login-section pd-tb60">
					<div class="container">
						<!--Form Box Start-->
       					<div class="cp-form-box cp-form-box2">
       						<h3>Submitted Videos</h3>
   							<div class="row">
	       						<div class="col-md-12 col-sm-12">
	       							<div class="inner-holder">
	       								<table class="table table-hover">
	       									<tr>
	       										<th>Video</th>
	       										<th>Title</th>
	       										<th>Created By</th>
	       										<th>Action</th>
	       									</tr>
	       									@foreach ($videos as $item)
	       									<tr>
	       										<td><a href="{{ env('APP_URL') }}?route=video&video={{ $item->id }}" target="_blank"><img style="width: 75px" src="http://img.youtube.com/vi/{{ $item->youtube_id }}/default.jpg" alt=""></a></td>
	       										<td>{{ $item->video_title }}</td>
	       										<td>{{ $item->video_created_by }}</td>
	       										<td>
	       											<a href="{{ env('APP_URL') . '?route=admin&detail=videos&action=edit&video=' . $item->id }}" class="btn btn-primary">Edit</a>
	       											<a href="{{ env('APP_URL') . '?route=admin&detail=delete-video&video=' . $item->id }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
	       										</td>
	       									</tr>
	       									@endforeach
	       								</table>
	       								{!! $videos->appends(['route' => 'admin', 'detail' => 'videos'])->render() !!}
	       							</div>
	       						</div>
   							</div>
       					</div><!--Form Box End-->
					</div>

				</section><!--Login Section End-->
			</div><!--Main Content End-->
@endsection