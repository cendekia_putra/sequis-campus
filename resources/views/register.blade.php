@extends('layout')

@section('content')
	<!--Banner Start-->
	<div class="cp_inner-banner">
		<div class="container">
			<div class="cp-inner-banner-holder">
				<h2>Register</h2>
			</div>
		</div>
	</div><!--Banner End-->

	<!--Main Content Start-->
	<div id="cp-main-content">
		<!--Login Section Start-->
		<section class="cp-register-section pd-tb60">
			<div class="container">
				<!--Form Box Start-->
				<div class="cp-form-box cp-form-box2">

					@include('partials.error_and_message')

					<h3>Register Now? or <a href="{{ env('APP_URL') . '?route=login' }}" style="color: #00b4c5">Login</a></h3>
					<form action="{{ env('APP_URL') }}?route=register" method="post">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6 col-sm-6">
   							<div class="inner-holder">
   								<h3>Your name*</h3>
   								<input type="text" name="name" placeholder="Enter Your Name..."  required>
   							</div>
   						</div>
   						<div class="col-md-6 col-sm-6">
   							<div class="inner-holder">
   								<h3>Email address*</h3>
   								<input type="text" placeholder="Email" name="email" required pattern="^[a-zA-Z0-9-\_.]+@[a-zA-Z0-9-\_.]+\.[a-zA-Z0-9.]{2,5}$">
   							</div>
   						</div>
   						<div class="col-md-6 col-sm-6">
							<div class="inner-holder">
   								<h3>Password*</h3>
   								<input type="password" placeholder="Password" name="password" required>
   							</div>
   						</div>
   						<div class="col-md-6 col-sm-6">
							<div class="inner-holder">
   								<h3>Retype Password*</h3>
   								<input type="password" placeholder="Password Confirmation" name="password_confirmation" required>
   							</div>
   						</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="inner-holder">
									<button type="submit" class="btn-submit btn-register" value="Submit">Sign Up Now</button>
								</div>
							</div>
						</div>
					</form>
				</div><!--Form Box End-->
			</div>

		</section><!--Login Section End-->
	</div><!--Main Content End-->
@endsection