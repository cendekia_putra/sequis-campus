@extends('layout')

@section('content')
	<!--Banner Start-->
			<div class="cp_inner-banner">
				<div class="container">
					<div class="cp-inner-banner-holder">
						<h2>Login</h2>
					</div>
				</div>
			</div><!--Banner End-->

			<!--Main Content Start-->
			<div id="cp-main-content">
				<!--Login Section Start-->
				<section class="cp-login-section pd-tb60">
					<div class="container">
						<!--Form Box Start-->
       					<div class="cp-form-box cp-form-box2">

							@include('partials.error_and_message')

       						<h3>Already Member? or <a href="{{ env('APP_URL') . '?route=register' }}" style="color: #00b4c5">Register</a></h3>
							<?php
								$intended = ($intendedUrl) ? urlencode($intendedUrl) :'';
							?>
       						<form action="{{ env('APP_URL') }}?route=login&intended_url={{ $intended }}" method="post">
       							{{ csrf_field() }}
       							<div class="row">
		       						<div class="col-md-6 col-sm-6">
		       							<div class="inner-holder">
		       								<h3>Email Address*</h3>
		       								<input type="text" placeholder="Email" name="email" required pattern="^[a-zA-Z0-9-\_.]+@[a-zA-Z0-9-\_.]+\.[a-zA-Z0-9.]{2,5}$">
		       							</div>
		       						</div>
		       						<div class="col-md-6 col-sm-6">
		       							<h3>Password*</h3>
       									<div class="inner-holder">
		       								<input type="password" placeholder="Password" name="password" required>
		       							</div>
		       						</div>
       								<div class="col-md-6 col-sm-6 col-xs-8">
       									<!-- <div class="inner-holder">
   											<h4>Login With</h4>
   											<ul class="cp-social-links">
   												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
   												<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
   												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
   											</ul>
		       							</div> -->
       								</div>
       								<div class="col-md-6 col-sm-6 col-xs-4">
       									<div class="inner-holder">
       										<button type="submit" class="btn-submit" value="Submit">Login</button>
       										<!-- <a href="#" class="lost-pw">Lost Password?</a> -->
       									</div>
       								</div>
       							</div>
       						</form>
       					</div><!--Form Box End-->
					</div>

				</section><!--Login Section End-->
			</div><!--Main Content End-->
@endsection