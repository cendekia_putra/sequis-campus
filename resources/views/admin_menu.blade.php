<div class="cp_inner-banner">
	<div class="container">
		<div class="cp-inner-banner-holder">
			<h2>Admin Page</h2>
			<ul class="breadcrumb">
				<li><a href="{{ env('APP_URL') }}?route=admin">Dashboard</a></li>
				<li><a href="{{ env('APP_URL') }}?route=admin&detail=users">Registered Users</a></li>
				<li><a href="{{ env('APP_URL') }}?route=admin&detail=videos">Videos</a></li>
				<li><a href="{{ env('APP_URL') }}?route=admin&detail=submit">Submit Video</a></li>
			</ul>
		</div>
	</div>
</div><!--Banner End-->