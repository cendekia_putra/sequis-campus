@extends('layout')

@section('content')
	<!--Banner Start-->
    <div class="cp_inner-banner">
        <div class="container">
            <div class="cp-inner-banner-holder">
                <h2>Sequis Online - Vote to Win</h2>
                <ul class="breadcrumb">
                    <li style="color: #00b4c5;font-size: 15px;line-height: 1.5;">Dukung tim favoritmu untuk mendapatkan <strong>golden ticket</strong> menuju babak <strong>Semi Final Marketition 2016!</strong></li>
                </ul><!--Breadcrumb End-->
            </div>
        </div>
    </div><!--Banner End-->

    <!--Main Content Start-->
    <div id="cp-main-content">
        <section class="cp-section pd-tb60">
            <div class="container">
                <!--Outer Holder Start-->
                <div class="cp-outer-holder">
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <ul>
                                <li><a href="{{ env('APP_URL') }}?route=howTo" style="color:#00b4c5"><i class="fa fa-question-circle"></i> How to Vote</a></li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="cp-categories-listed-outer">
                                <ul class="row">
                                	@foreach ($videos as $item)
                                    <li class="col-md-4 col-sm-4 col-xs-12">
                                        <!-- Video Item Start-->
                                        <div class="cp-video-item">
                                            <figure class="cp-thumb">
                                                <img src="http://img.youtube.com/vi/{{ $item->youtube_id }}/mqdefault.jpg" alt="">
                                                <figcaption class="cp-caption">
                                                    <a href="{{ env('APP_URL') }}?route=video&video={{ $item->id }}" class="play-video">Play</a>
                                                </figcaption>
                                            </figure>
                                            <div class="cp-text" style="min-height: 126px;">
                                                <h4><a href="{{ env('APP_URL') }}?route=video&video={{ $item->id }}">{{ $item->video_title }}</a></h4>
                                                <ul class="cp-meta-list">
                                                    <li>{{ date('l, F d, Y', strtotime($item->video_submitted)) }}</li>
                                                    <li>by {{ $item->video_created_by }}, <span>{{ $item->views }} Views</span>, <span>{{ $item->likes }} Likes</span></li>
                                                </ul>
                                            </div>
                                        </div><!-- Video Item End-->
                                    </li>
                                    @endforeach
                                </ul>
                            </div>

                            <!--Pagination Start-->
                            <div class="cp-pagination-row">
                                  <ul class="pagination">
                                    <li><a href="{{ env('APP_URL') }}?view_all=1">View all: </a></li>
                                    <li>
                                      <a href="{{ $videos->previousPageUrl() }}" aria-label="Previous">
                                        <i class="fa fa-caret-square-o-left"></i>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="{{ $videos->nextPageUrl() }}" aria-label="Next">
                                       <i class="fa fa-caret-square-o-right"></i>
                                      </a>
                                    </li>
                                  </ul>
                            </div><!--Pagination End-->
                        </div>
                        <div class="col-md-3">
                            <aside class="cp_sidebar-outer">
                                <!--Widget Item Start-->
                                <div class="widget widget-recent-post">
                                    <div class="cp-heading-outer">
                                        <h2>Most Popular</h2>
                                        <ul class="cp-listed">
                                            <li>&nbsp;</li>
                                        </ul>
                                    </div>
                                    <ul>
                                    	@foreach ($mostPopulars as $item)
                                        <li>
                                            <div class="cp-holder">
                                                <div class="cp-thumb2">
                                                    <a href="{{ env('APP_URL') }}?route=video&video={{ $item->id }}"><img style="width: 75px" src="http://img.youtube.com/vi/{{ $item->youtube_id }}/default.jpg" alt=""></a>
                                                </div>
                                                <div class="cp-text">
                                                    <h5><a href="{{ env('APP_URL') }}?route=video&video={{ $item->id }}">{{ $item->video_title }}</a></h5>
                                                    <ul class="cp-meta-list">
                                                        <li>{{ date('l, F d, Y', strtotime($item->video_submitted)) }}</li>
                                                        <li>by {{ $item->video_created_by }}, <span>{{ $item->likes }} Likes</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <!-- <a href="#" class="recent-more-listed">More popular content »</a> -->
                                </div><!--Widget Item End-->
                            </aside>
                        </div>
                    </div>
                </div><!--Outer Holder End-->

            </div>
        </section>
    </div><!--Main Content End-->
@endsection